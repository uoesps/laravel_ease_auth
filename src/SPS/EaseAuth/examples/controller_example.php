<?php

class UsersController extends BaseController
{

	/**
	 * Is the current user logged in?
	 *
	 * @var bool
	 */
	protected $loggedIn = false;

    /*
     * If the user is not locally authenticated we attempt to log them in,
     * if that fails we send them back to the EASE login page
     */
    public function loginAction()
    {
        if (Auth::check()) {
            $this->loggedIn = true;
        } elseif ($username = Auth::remote()) {
            $credentials = array(
                'username' => $username
            );

            if (Auth::attempt($credentials)) {
                $this->loggedIn = true;
            }
        }
        
        if ($this->loggedIn) {
            return Redirect::intended('dashboard');
        } else {
            return Redirect::away(Config::get('auth.ease_login'));
        }
    }
    
    /*
     * Log the user out of the local session
     * then send them off to the EASE logout page
     */
    public function logoutAction()
    {
        Auth::logout();

        return Redirect::away(Config::get('auth.ease_logout'));
    }
    
    public function viewAction()
    {
        return View::make('users.view');
    }

}
