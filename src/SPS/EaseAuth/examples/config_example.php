<?php

return array(
    'ease_login' => 'https://www.ease.ed.ac.uk/',
    'ease_logout' => 'https://www.ease.ed.ac.uk/logout/logout.cgi'
);
